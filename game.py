import math
import random
import pygame
error = 0
num = 0
avg = -1500
integral = 0
derivative = 0
lastError = 0
TARGET_VELOCITY = 600
velocity = 0
flywheel_KP= 0.8
flywheel_KI= 0.055
flywheel_KD = 0.0075
reset = False
pygame.init()
def v_random():
  global velocity
  velocity += random.randint(random.randint(-600,0),0)
def intakePID():
    global error, integral, derivative, lastError, velocity, TARGET_VELOCITY, flywheel_KP
    global flywheel_KI, flywheel_KD, avg, num, reset

    error = TARGET_VELOCITY - velocity
    integral += error
    derivative = error - lastError
    lastError = error
    power = flywheel_KP * error + flywheel_KI * integral + flywheel_KD * derivative

    velocity = power
    num += 1
    velocity += random.randint(-10,0)
    avg = (((velocity-600)/600)+num*avg)/num
    
    velocity = round(velocity, 5)
    if (velocity == 600 and reset == False):
        avg = 0
        reset = True
    print({"Velocity ":velocity ,"KP: ": flywheel_KP ,"KI: " :flywheel_KI ,"KD: ":flywheel_KD, "Avg: ": round(100*avg, 2)})
clock = pygame.time.Clock()
while True:
  clock.tick(50)
  intakePID()
  for event in pygame.event.get():
      if event.type == pygame.QUIT:
          pygame.quit()
      keys = pygame.key.get_pressed()
      if(keys[pygame.K_0]):
          v_random()
          num += 1
          avg = (((velocity-600)/600)+num*avg)/num
          velocity = round(velocity, 5)
          print({"Velocity ":velocity ,"KP: ": flywheel_KP ,"KI: " :flywheel_KI ,"KD: ":flywheel_KD, "Avg Error: ": 100*avg})
      if(keys[pygame.K_LEFT]):
          flywheel_KD -= 0.0001
      if(keys[pygame.K_RIGHT]):
          flywheel_KD += 0.0001
      if(keys[pygame.K_q]):
          avg = 0
